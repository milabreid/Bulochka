import discord
import random
import os
import yaml
from yaml import CLoader as Loader

intents = discord.Intents.default()
intents.message_content = True

client = discord.Client(intents=intents)

@client.event
async def on_ready():
    print(f"We have logged in as {client.user}")

@client.event
async def on_message(message):
    if message.author == client.user:
        return

    await message.channel.send(
        help_message(message.content)
    )

def help_message(content):
    content = content.lower()

    with open('random_answers.txt') as f:
        random_answers = f.readlines()

    with open("answers.yaml") as g:
       answers = yaml.load(g, Loader)

    for message in answers.keys():
        if message.startswith(content):
            return answers[message]

    return random.choice(random_answers)

if __name__ == '__main__':
    client.run(os.getenv("bot_token"))


def test_list():
    with open('random_answers.txt') as f:
        answers = f.readlines()
    assert help_message("boo") in answers

